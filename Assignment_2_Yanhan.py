#################################################
#    SatrtData: 2021/10/18                      #
#    EndData:2021/10/21                         #
#    Name: YanhanChen                           #
#    Teacher: Omer Mohamed                      #
#################################################
from typing import Sized


class User:
    def __init__(self) -> None:
        self.name = "None"
        self.email = "None"
        self.address = "None"
    def __str__(self):
        self.output = (
            "Your user name is: "+ str(self.getName())+ "\n"+
            "Your email is: "+ str(self.getEmail())+ "\n"+
            "Your address is: "+ str(self.getAddress())+ "\n"
        )
        return self.output

    def getName(self):
        return self.name
    def getEmail(self):
        return self.email
    def getAddress(self):
        return self.address
    
    def setName(self,n):
        self.name=n
    def setEmail(self,e):
        self.email=e
    def setAddress(self,a):
        self.address=a
        

class Pizza:
    def __init__(self) -> None:
        self.size = "None"
        self.number = "None"
    def __str__(self) -> str:
        self.output = (
            str(self.getSize())+ "\n"+
            "The number of pizza is/are "+ str(self.getNumber())+ "\n"+
            "The price are "+ str(self.getPrice())+ " CDN"
            )
        return self.output

    def getSize(self):
        if self.size == 1:
            return("The size of pizza is Small.")
        elif self.size == 2:
            return("The size of pizza is Medium.")
        elif self.size == 3:
            return("The size of pizza is Large.")
    def getNumber(self):
        return self.number
    def getPrice(self):
        if self.size == 1:
            return 10*self.number
        elif self.size == 2:
            return 12*self.number
        elif self.size == 3:
            return 15*self.number

    def setSize(self,s):
        self.size = s
    def setNumber(self,n):
        self.number = n

#set the User's data
user1 = User()
user1.setName(str(input("What's your name?  ")))
user1.setEmail(str(input("What's your email?  ")))
user1.setAddress(str(input("What's your address?  ")))

#set the Pizza's data
pizza1 = Pizza()
pizza1.setSize(int(input("""Please chosse your pizza size
1 = Small
2 = Medium
3 = Large
""")))
pizza1.setNumber(float(input("how many pizza you want? ")))

print(user1)
print(pizza1)